import {User} from './Users.models';

export class Commentaire{
    uuid_comment: string;
    uuid_post: string;
    uuid_user: string;
    comment_date: Date;
    comment_edit_date: Date;
    comment: string;

    constructor(
        uuid_comment?: string,
        uuid_post?: string,
        uuid_user?: string,
        comment_date?: Date,
        comment_edit_date?: Date,
        comment?: string
    ) {
        this.uuid_comment = uuid_comment;
        this.uuid_post = uuid_post;
        this.uuid_user = uuid_user;
        this.comment_date = comment_date;
        this.comment_edit_date = comment_edit_date;
        this.comment = comment;
    }
}

export class CommentAndAuthor{
  comment: Commentaire;
  author: User;

  constructor(comment?: Commentaire, author?: User){
    this.comment = comment;
    this.author = author;
  }
}
