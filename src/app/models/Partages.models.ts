
export class Partage{
    uuid_share: string;
    uuid_post: string;
    uuid_share_user: string;
    uuid_author_user: string;
    share_date: Date;

    constructor(
        uuid_share?: string,
        uuid_post?: string,
        uuid_share_user?: string,
        uuid_author_user?: string,
        share_date?: Date
    ) {
        this.uuid_share = uuid_share;
        this.uuid_post = uuid_post;
        this.uuid_share_user = uuid_share_user;
        this.uuid_author_user = uuid_author_user;
        this.share_date = share_date;
    }
}
