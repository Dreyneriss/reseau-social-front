export class Like{
    uuid_user: string;
    uuid_post: string;

    constructor(
        uuid_user?: string,
        uuid_post?: string,
    ) {
        this.uuid_user = uuid_user;
        this.uuid_post = uuid_post;
    }
}
