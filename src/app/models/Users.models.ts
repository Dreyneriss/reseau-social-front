
export class User{
    uuid_user: string;
    user_name: string;
    user_login: string;
    user_password: string;
    user_imageurl: string;
    user_location: string;
    user_birthdate: Date;
    user_email: string;
    user_description: string;
    id_keycloak: string;


    constructor(
        uuid_user?: string,
        user_name?: string,
        user_login?: string,
        user_password?: string,
        user_imageurl?: string,
        user_location?: string,
        user_birthdate?: Date,
        user_email?: string,
        user_description?: string,
        id_keycloak?: string,
    ) {
        this.uuid_user = uuid_user;
        this.user_name = user_name;
        this.user_login = user_login;
        this.user_password = user_password;
        this.user_imageurl = user_imageurl;
        this.user_location = user_location;
        this.user_birthdate = user_birthdate;
        this.user_email = user_email;
        this.user_description = user_description;
        this.id_keycloak = id_keycloak;
    }
}
