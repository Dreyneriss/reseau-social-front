import {Partage} from './Partages.models';
import {User} from './Users.models';

export class Post{
    post_authorName: string;
    uuid_post: string;
    uuid_user: string;
    post_content: string;
    post_imagename: string;
    post_date: Date;
    post_edit_date: Date;
    post_nb_like: number;
    img: Object;

    constructor(
        postAuthorName?: string,
        uuid_post?: string,
        uuid_user?: string,
        post_content?: string,
        post_imagename?: string,
        post_date?: Date,
        post_edit_date?: Date,
        post_nb_like?: number,
        img?: Object
    ) {
        this.post_authorName = postAuthorName;
        this.uuid_post = uuid_post;
        this.uuid_user = uuid_user;
        this.post_content = post_content;
        this.post_imagename = post_imagename;
        this.post_date = post_date;
        this.post_edit_date = post_edit_date;
        this.post_nb_like = post_nb_like;
        this.img = img;
    }
}

export class PostStats{
    post: Post;
    nbComments: number;
    nbShares: number;
    nbLikes: number;

    constructor(
        post?: Post,
        nbComments?: number,
        nbShares?: number,
        nbLikes?: number,
    ) {
        this.post = post;
        this.nbComments = nbComments;
        this.nbShares = nbShares;
        this.nbLikes = nbLikes;
    }

}

export class PostAndShares{
    post: Post;
    author: User;
    share: Partage;
    share_user: User;
    date: Date;
    nbComments: number;
    nbShares: number;
    nbLikes: number;

    constructor(
        post?: Post,
        author?: User,
        share?: Partage,
        share_user?: User,
        date?: Date,
        nbComments?: number,
        nbShares?: number,
        nbLikes?: number
    ) {
        this.post = post;
        this.author = author;
        this.share = share;
        this.share_user = share_user;
        this.date = date;
        this.nbComments = nbComments;
        this.nbShares = nbShares;
        this.nbLikes = nbLikes;
    }

    setPost(post: Post){
        this.post = post;
    }
    setShare(share: Partage){
        this.share = share;
    }
    setAuthor(author: User){
        this.author = author;
    }
    setShareUser(user: User){
        this.share_user = user;
    }
    setNbComment(nb: number){
        this.nbComments = nb;
    }
    setNbLike(nb: number){
        this.nbLikes = nb;
    }
    setNbShare(nb: number){
        this.nbShares = nb;
    }
}

export class PictureId{
    name: string;
    status: number;
    message: string;
    constructor(name?:string, status?:number, message?: string){
        this.name = name;
        this.status = status;
        this.message = message;
    }
}