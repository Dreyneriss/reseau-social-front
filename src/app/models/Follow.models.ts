export class Follow{
    uuid_follower: string;
    uuid_user_followed: string;

    constructor(
        uuid_follower?: string,
        uuid_user_followed?: string,
    ) {
        this.uuid_follower = uuid_follower;
        this.uuid_user_followed = uuid_user_followed;
    }
}
export class FollowCount{
    count: number;

    constructor(
        count?: number,
    ) {
        this.count = count;
    }
}
