import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignupComponent} from './auth/signup/signup.component';
import {SigninComponent} from './auth/signin/signin.component';
import {ListPostComponent} from './list-post/list-post.component';
import {NewPostComponent} from './list-post/new-post/new-post.component';
import {DetailsComponent} from './list-post/details-post/details/details.component';
import {ProfileComponent} from './profile/profile.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { ProfileFollowsComponent } from './profile/profile-follows/profile-follows.component';
import {AuthGuard} from './utility/app.guard';

const appRoutes: Routes = [
    { path: 'auth/signup', component: SignupComponent , canActivate: [AuthGuard]},
    { path: 'auth/signin', component: SigninComponent , canActivate: [AuthGuard]},
    { path: 'posts', component: ListPostComponent, canActivate: [AuthGuard]},
    { path: 'posts/details/:uuid', component: DetailsComponent, canActivate: [AuthGuard]},
    { path: 'posts/new', component: NewPostComponent , canActivate: [AuthGuard]},
    { path: 'profile/profile/:id', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'profile/edit', component: EditProfileComponent , canActivate: [AuthGuard]},
    { path: 'profile/follow/:id', component: ProfileFollowsComponent , canActivate: [AuthGuard]},
    { path: '', redirectTo: 'posts', pathMatch: 'full'},
    { path: '**', redirectTo: 'posts'}
];
@NgModule({
     imports: [RouterModule.forRoot(appRoutes)],
     exports: [RouterModule]
 })
export class AppRoutingModule { }
