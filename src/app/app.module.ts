import {NgModule, LOCALE_ID, APP_INITIALIZER} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ListPostComponent } from './list-post/list-post.component';
import { NewPostComponent } from './list-post/new-post/new-post.component';
import {PostsService} from './services/posts.service';
import { HeaderComponent } from './header/header.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing-modules';
import {AuthService} from './services/auth.service';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DetailsComponent } from './list-post/details-post/details/details.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { ProfileFollowsComponent } from './profile/profile-follows/profile-follows.component';
import { ProfilePostsComponent } from './profile/profile-posts/profile-posts.component';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {initializeKeycloak} from './utility/app.init';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    ListPostComponent,
    NewPostComponent,
    HeaderComponent,
    SigninComponent,
    SignupComponent,
    DetailsComponent,
    ProfileComponent,
    EditProfileComponent,
    ProfileFollowsComponent,
    ProfilePostsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
    AppRoutingModule,
    KeycloakAngularModule
  ],
  providers: [
      AuthService,
      PostsService,
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
