import {Component, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Post, PostAndShares} from '../models/Posts.models';
import {PostsService} from '../services/posts.service';
import {Router} from '@angular/router';
import {UsersService} from '../services/users.service';
import {User} from '../models/Users.models';
import {faHeart} from '@fortawesome/free-solid-svg-icons/faHeart';
import {faComment} from '@fortawesome/free-solid-svg-icons/faComment';
import {faShare} from '@fortawesome/free-solid-svg-icons/faShare';
import {PartageService} from '../services/partage.service';
import {CommentService} from '../services/comment.service';
import {LikeService} from '../services/like.service';
import {FollowService} from '../services/follow.service';
import { Partage } from '../models/Partages.models';
import { Follow } from '../models/Follow.models';
import { DomSanitizer } from '@angular/platform-browser';
import {KeycloakService} from 'keycloak-angular';


@Component({
  selector: 'app-list-post',
  templateUrl: './list-post.component.html',
  styleUrls: ['./list-post.component.css']
})
export class ListPostComponent implements OnInit, OnDestroy {
  faHeart = faHeart;
  faComment = faComment;
  faShare = faShare;

  lovedPost: boolean;

  followedPosts : PostAndShares[];
  otherPosts : PostAndShares[];

  users: User[] = [];
  uuidUser: string;
  user_name: string;
  editorPost: User;
  @Output() userid: string;
  buttonLove = 'neutre';
  authId = '';

  constructor(private postService: PostsService,
              private router: Router,
              private userService: UsersService,
              private partageService: PartageService,
              private commentService: CommentService,
              private likeService: LikeService,
              private followService: FollowService,
              private sanitizer: DomSanitizer,
              private keycloak: KeycloakService) {
  }

  ngOnInit(): void {
    const userK = this.keycloak.getKeycloakInstance();
    const userId = userK.subject.toString();

    this.userService.getUserByKeycloak(userId).subscribe(user => {
      if (user.length > 0){
        this.authId = user[0].uuid_user;
        this.isWaited();
      }else{ //user exists in keycloak but not in the user table. Must be created
        const first_name = userK.profile.firstName;
        const last_name = userK.profile.lastName;
        const login = userK.profile.username;
        const email = userK.profile.email;

        this.userService.addUser(first_name, last_name, login, email, userId);
        this.ngOnInit();
      }
    });
  }
  // tslint:disable-next-line:typedef

  isWaited(){
    this.followedPosts = [];
    this.otherPosts = [];

    this.userService.getUsers().subscribe((user) => {

      this.users = user;
      this.editorPost = new User(this.uuidUser, this.user_name);
      this.uuidUser = user[1].uuid_user.toString();
      this.user_name = user[1].user_name.toString();
      return user;
    });
    this.followService.getFollows(this.authId).subscribe((follows) => {
      console.log(follows);
      this.postService.getPosts().subscribe((posts) => {
        posts.forEach(post => {
          this.userService.getUserById(post.uuid_user).subscribe(user => {
            this.commentService.getNbCommentsFromPost(post.uuid_post).subscribe((nbComment) => {
              this.partageService.getNbSharesOfPost(post.uuid_post).subscribe((nbPartage) => {
                this.likeService.getLikesFromPost(post.uuid_post).subscribe((likes) => {
                  const nbLike = likes.length;
                  this.lovedPost = false;
                  likes.forEach((like)=>{
                    if(like.uuid_user == this.authId){
                      this.lovedPost = true;
                    }
                  });
                  if(this.isFollowed(post.uuid_user, follows)){
                    this.followedPosts.push(new PostAndShares(post, user[0], undefined, undefined, post.post_date,
                      nbComment[0].count,nbPartage[0].count, nbLike));
                  }else{
                    this.otherPosts.push(new PostAndShares(post, user[0], undefined, undefined, post.post_date,
                      nbComment[0].count,nbPartage[0].count, nbLike));
                  }
                });
              });
            });
          });
          if (post.post_imagename != undefined){ // shall disappear
            this.postService.downloadFile(post.post_imagename).subscribe((data: Blob) => {
              const unsafeUrl = URL.createObjectURL(data);
              post.img = this.sanitizer.bypassSecurityTrustUrl(unsafeUrl);
            });
          }
        });
        return posts;
      });

      this.partageService.getShares().subscribe((shares) => {
        shares.forEach(share => {
          this.userService.getUserById(share.uuid_share_user).subscribe((share_user) => {
            if (share.uuid_post != undefined){
              this.postService.getPostById(share.uuid_post).subscribe((post) => {
                this.userService.getUserById(share.uuid_author_user).subscribe((post_author) => {
                  this.commentService.getNbCommentsFromPost(share.uuid_post).subscribe(nbComment => {
                    this.partageService.getNbSharesOfPost(share.uuid_post).subscribe(nbPartage => {
                      this.likeService.getNbLikeForPost(share.uuid_post).subscribe(nblike => {
                        if (this.isFollowed(share.uuid_share_user, follows)){
                          this.followedPosts.push(new PostAndShares(post[0], post_author[0], share, share_user[0],
                              share.share_date, nbComment[0].count, nbPartage[0].count, nblike[0].count));
                        }else{
                          this.otherPosts.push(new PostAndShares(post[0], post_author[0], share, share_user[0],
                              share.share_date, nbComment[0].count, nbPartage[0].count, nblike[0].count));
                        }
                      });
                    });
                  });
                });
                if (post[0].post_imagename != undefined){ // shall disappear
                  this.postService.downloadFile(post[0].post_imagename).subscribe((data: Blob) => {
                    const unsafeUrl = URL.createObjectURL(data);
                    post[0].img = this.sanitizer.bypassSecurityTrustUrl(unsafeUrl);
                  });
                }
              });
            }else{
              if (this.isFollowed(share.uuid_share_user, follows)){
                this.followedPosts.push(new PostAndShares(undefined, undefined, share, share_user[0], share.share_date,
                    undefined, undefined, undefined));
              }else{
                this.otherPosts.push(new PostAndShares(undefined, undefined, share, share_user[0], share.share_date,
                    undefined, undefined, undefined));
              }
            }
          });
        });
      });
    });
    this.postService.emitPosts();
  }
  isFollowed(idUser: string, follows: Follow[]): boolean{
    let found = false;
    follows.forEach(follow => {
      if (idUser == follow.uuid_user_followed){
        found = true;
      }
    });
    return found;
  }

  getSortedPosts(posts: PostAndShares[]): PostAndShares[]{
    return posts.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
  }

  // tslint:disable-next-line:typedef
  onDeletePost(post: Post) {
    this.postService.removePost(post);
  }
  onDeleteShare(share: Partage){
    this.partageService.deleteFollow(share.uuid_share);
  }
  // tslint:disable-next-line:typedef
  onShare(post: PostAndShares){
    console.log("post to share : ", post);
    this.partageService.addPartage(post.post.uuid_post, this.authId, post.post.uuid_user);
  }
  // tslint:disable-next-line:typedef
  onLove(post: PostAndShares){
    if(this.lovedPost){
      this.lovedPost = false;
      post.nbLikes -= 1;
      post.post.post_nb_like -= 1;
      this.likeService.deleteLike(this.authId,post.post.uuid_post);
      this.ngOnInit();
    }else{
      this.lovedPost = true;
      post.nbLikes += 1;
      post.post.post_nb_like += 1;
      this.likeService.addLike(this.authId,post.post.uuid_post);
      this.ngOnInit();
    }
  }
  // tslint:disable-next-line:typedef
  onDontlove(post: Post){
    this.postService.dontLove(post);
    this.buttonLove = 'dislike';
  }
  ngOnDestroy(): void {
    // this.postSubscription.unsubscribe();
  }

  onProfile(login: string){
    this.router.navigate(['profile/profile/' + login]);
  }
}
