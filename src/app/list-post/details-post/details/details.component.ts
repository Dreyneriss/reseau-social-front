import { Component, OnInit } from '@angular/core';
import {User} from '../../../models/Users.models';
import {PostStats} from '../../../models/Posts.models';
import {Commentaire, CommentAndAuthor} from '../../../models/Commentaires.model'
import {PostsService} from '../../../services/posts.service';
import {UsersService} from '../../../services/users.service';
import {CommentService} from '../../../services/comment.service';
import {PartageService} from '../../../services/partage.service';
import {LikeService} from '../../../services/like.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, FormArray, FormControl, Validators} from '@angular/forms';
import { faHeart } from '@fortawesome/free-solid-svg-icons/faHeart';
import { faComment } from '@fortawesome/free-solid-svg-icons/faComment';
import { faShare } from '@fortawesome/free-solid-svg-icons/faShare';
import {KeycloakService} from 'keycloak-angular';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  changeContentForm: FormGroup;
  newCommentForm: FormGroup;
  
  faHeart = faHeart;
  faComment = faComment;
  faShare = faShare;

  editComment: FormArray[];
  comments = new FormArray([]);

  profilePicture = '/assets/img/chalderiate.jpg';
  authId = "";
  lovedPost: boolean;

  post: PostStats;
  postId: string;
  postUser: User;

  commentaires: CommentAndAuthor[];

  authorView: boolean;

  constructor(private formBuilder: FormBuilder,
      private postsService: PostsService,
      private usersService: UsersService,
      private commentService: CommentService,
      private partageService: PartageService,
      private likeService: LikeService,
      private router: Router,
      private route: ActivatedRoute,
              private keycloak: KeycloakService,
              private userService: UsersService,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    const userId = this.keycloak.getKeycloakInstance().subject.toString();
    this.userService.getUserByKeycloak(userId).subscribe(user => {
      console.log(user);
      if (user.length > 0){
        this.authId = user[0].uuid_user;
      }
      console.log('testetst ' + this.authId);
      this.isWaited();
    });
  }
  isWaited(){
    this.changeContentForm = this.formBuilder.group({
      content: new FormControl()
    });
    this.newCommentForm = this.formBuilder.group({
      comment: new FormControl()
    });
    this.postId = this.route.snapshot.params['uuid'];
    this.post = new PostStats();
    this.post.nbComments = 0;
    this.commentaires = [];

    this.postsService.getPostById(this.postId).subscribe((post) => {
      this.changeContentForm.get('content').setValue(post[0].post_content);
      this.post.post = post[0];
      if (post[0].post_imagename != undefined){ // shall disappear
        this.postsService.downloadFile(post[0].post_imagename).subscribe((data: Blob) => {
          const unsafeUrl = URL.createObjectURL(data);
          post[0].img = this.sanitizer.bypassSecurityTrustUrl(unsafeUrl);
        });
      }
      this.post.nbLikes = (post[0].post_nb_like == undefined)? 0 : post[0].post_nb_like;
      this.authorView = this.authId == post[0].uuid_user;
      this.partageService.getNbSharesOfPost(post[0].uuid_post).subscribe((count) => {
        this.post.nbShares = count[0].count;
      });
      this.usersService.getUserById(post[0].uuid_user).subscribe((user) => {
        this.postUser = user[0];
      });
      this.likeService.getLikesFromPost(this.postId).subscribe((likes) => {
        this.post.nbLikes = likes.length;
        this.post.post.post_nb_like = likes.length;
        this.lovedPost = false;
        likes.forEach((like)=>{
          if(like.uuid_user == this.authId){
            this.lovedPost = true;
          }
        });
      });
      return post;
    });

    this.commentService.getCommentsFromPost(this.postId).subscribe((comments) => {
      this.getSortedComments(comments).forEach((comment) => {
        this.usersService.getUserById(comment.uuid_user).subscribe((user) => {
          this.commentaires.push(new CommentAndAuthor(comment,user[0]));
          if(this.commentaires.length == comments.length){
            this.sortCommentsAndAuthors();
          }
          return user;
        });
        this.comments.push(new FormControl(comment.comment));
      });
      this.post.nbComments = comments.length;
      return comments;
    });
  }
  getSortedComments(comments : Commentaire[]) : Commentaire[]{
    return comments.sort((a,b) => new Date(a.comment_date).getTime() - new Date(b.comment_date).getTime());
  }

  sortCommentsAndAuthors(){
    this.commentaires = this.commentaires.sort((a,b) => new Date(a.comment.comment_date).getTime() - new Date(b.comment.comment_date).getTime());
  }

  onLove(){
    if(this.lovedPost){
      this.lovedPost = false;
      this.post.nbLikes -= 1;
      this.post.post.post_nb_like -= 1;
      this.likeService.deleteLike(this.authId,this.postId);
      this.ngOnInit();
    }else{
      this.lovedPost = true;
      this.post.nbLikes += 1;
      this.post.post.post_nb_like += 1;
      this.likeService.addLike(this.authId,this.postId);
      this.ngOnInit();
    }
  }

  onShare(){
    this.partageService.addPartage(this.postId, this.authId, this.postUser.uuid_user);
  }

  onEditContent(){
    const content = this.changeContentForm.get('content').value;
    console.log("content : ", content);
    this.post.post.post_content = content;
    this.postsService.editPostContent(this.post.post);
  }

  onPostComment(){
    const comment = this.newCommentForm.get('comment').value;
    console.log("comment : ", comment);
    this.commentService.postComment(this.authId,this.postId,comment);
  }

  onEditComment(i : number,commentObject : Commentaire){
    const newComment = this.comments.controls[i].value;

    commentObject.comment = newComment;

    this.commentService.editComment(commentObject);
  }

  onRemoveComment(commentObject : Commentaire){
    this.commentService.deleteComment(commentObject.uuid_comment);
  }

  onRemovePost(){
    this.postsService.removePost(this.post.post);
  }

  onProfile(login: string){
    this.router.navigate(['profile/profile/'+login]);
  }
}
