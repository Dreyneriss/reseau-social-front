import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {PostsService} from '../../services/posts.service';
import {Router} from '@angular/router';
import {KeycloakService} from 'keycloak-angular';
import {Post} from '../../models/Posts.models';
import {UsersService} from '../../services/users.service';
import {ListPostComponent} from '../list-post.component';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  authId = "cdc8b40e-2618-445e-af4a-b014b3304da0";

  postForm: FormGroup;
  fileIsUploading = false;
  fileURL: string;
  fileUploaded = false;
  loadedFile;

  constructor(private formBuilder: FormBuilder,
              private postService: PostsService,
              private router: Router,
              private keycloak: KeycloakService,
              private userService: UsersService,
              ) {
  }

  ngOnInit(): void {
    const userId = this.keycloak.getKeycloakInstance().subject.toString();
    this.userService.getUserByKeycloak(userId).subscribe(user => {
      console.log(user);
      if (user.length > 0){
        this.authId = user[0].uuid_user;
      }
      console.log('testetst ' + this.authId);
      this.initForm();
    });
  }
  initForm() {
    this.postForm = this.formBuilder.group({
      content: new FormControl()
    });
  }

  onSavePost() {
    // const title = this.postForm.get('title').value;
    // const content = this.postForm.get('content').value;
    // const createAt = new Date().toString();
    // const like = 0;
    // const dislike = 0;
    // const newPost = new Post(title, content, createAt, like, dislike);
    // if (this.fileURL && this.fileURL !== '') {
    //   newPost.photo = this.fileURL;
    // }
    // this.postService.createNewPost(newPost);
    // this.router.navigate(['/posts']);
  }

  // tslint:disable-next-line:typedef
  onUploadFile() {
    if(this.loadedFile != undefined){
      const content = this.postForm.get('content').value;
      this.fileIsUploading = true;
      this.postService.uploadFile(this.loadedFile, this.authId, content).then(
          (url: string) => {
            console.log("url : ", url);
            this.fileURL = url;
            this.fileIsUploading = false;
            this.fileUploaded = true;
          }
      );
    }
  }

  // tslint:disable-next-line:typedef
  detectFiles(event) {
    //this.onUploadFile(event.target.files[0]);
    this.loadedFile = event.target.files[0];
  }
}
