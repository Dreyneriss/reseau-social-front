import {KeycloakService} from 'keycloak-angular';

export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean>{
    return () =>
        keycloak.init({
            config: {
                url: 'https://keycloak.picarrot.live/auth',
                // url: 'https://keycloak.picarrot.live/auth',
                realm: 'users',
                clientId: 'api-auth',
            },
            
            initOptions: {
                redirectUri: 'http://app3.picarrot.live',
                checkLoginIframe: true,
                checkLoginIframeInterval: 25,
                responseMode: 'query',

            },
            loadUserProfileAtStartUp: true,
            
            

        });
}
