import { Component, OnInit } from '@angular/core';
import {User} from '../models/Users.models';
import {UsersService} from '../services/users.service';
import {FollowService} from '../services/follow.service';
import {ActivatedRoute, Router} from '@angular/router';
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user = '';
  authProfile: User;
  authId = '';
  profile: User;
  paramLogin: string;
  profilePicture = "/assets/img/chalderiate.jpg";

  userSession: boolean;
  userFollowed: boolean;

  nbFollow: number;
  nbFollower: number;

  followMessage: "Suivre cet utilisateur";
  unfollowMessage: "Ne plus suivre cet utilisateur";

  constructor(private followService: FollowService,
              private usersService: UsersService,
              private router: Router,
              private route: ActivatedRoute,
              private keycloakService: KeycloakService,
              ) { }

  ngOnInit(): void {
    const userId = this.keycloakService.getKeycloakInstance().subject.toString();
    this.usersService.getUserByKeycloak(userId).subscribe(user => {
      console.log(user);
      if (user.length > 0){
        this.authId = user[0].uuid_user;
      }
      console.log('testetst ' + this.authId);
      this.isWaited();
    });
  }
  isWaited(){
    this.initializeUserOption();
    this.usersService.getUserById(this.authId).subscribe((user) => {
      this.authProfile = user[0];

      this.paramLogin = this.route.snapshot.params['id']; //even if I actually ask the login

      if( (this.paramLogin === "user") || (this.paramLogin === this.authProfile.user_login) ){
        this.userSession = true;
        this.profile = this.authProfile;

        this.loadInfos();
      }else{
        this.userSession = false;

        this.usersService.getUserByLogin(this.paramLogin).subscribe((profile) => {
          if(profile.length == 0){
            this.router.navigate(['profile/user']);
          }else{
            this.profile = profile[0];

            this.loadInfos();
            this.checkIfFollow()

            return profile;
          }
        });
      }

      return user;
    });
  }
  loadInfos(){
    this.followService.getNbFollow(this.profile.uuid_user).subscribe((data) => {
      this.nbFollow = data[0].count;
    });
    this.followService.getNbFollower(this.profile.uuid_user).subscribe((data) => {
      this.nbFollower = data[0].count;
    });
    if( (this.profile.user_imageurl != undefined) && (this.profile.user_imageurl != "") ){
      //vérifier si l'image existe
      this.profilePicture = this.profile.user_imageurl;
    }
  }

  editProfile(){
    this.router.navigate(['profile/edit']);
  }

  seeFollows(){
    this.router.navigate(['profile/follow/',this.profile.user_login]);
  }

  checkIfFollow(){
    this.followService.getFollows(this.authId).subscribe((follows) => {
      follows.forEach(follow => {
        if(follow.uuid_user_followed === this.profile.uuid_user){
          this.userFollowed = true;
        }
      });
      if(this.userFollowed === undefined){
        this.userFollowed = false;
      }
    });
  }

  followClick(){
    if (this.userFollowed){
      this.followService.deleteFollow(this.authId, this.profile.uuid_user);
      // unauthorized
    }else if(!this.userFollowed){ //cause in case of undefined...
      this.followService.addFollow(this.authId, this.profile.uuid_user);
      // unauthorized
    }
  }

  private initializeUserOption(): void{
    this.user = this.keycloakService.getUsername();
  }
  logout(): void {
    this.keycloakService.logout('http://app3.picarrot.live');
  }
}
