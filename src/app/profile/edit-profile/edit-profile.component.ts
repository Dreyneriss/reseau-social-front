import { Component, OnInit } from '@angular/core';
import {User} from '../../models/Users.models';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  saveChanges: FormGroup;
  profile: User;
  new_profile: User;
  authID = '';
  nbFollow: number;
  nbFollower: number;

  constructor(private formBuilder: FormBuilder,
              private userService: UsersService,
              private router: Router,
              private keycloak: KeycloakService) { }

  ngOnInit(): void {
    const userId = this.keycloak.getKeycloakInstance().subject.toString();
    this.userService.getUserByKeycloak(userId).subscribe(user => {
      console.log(user);
      if (user.length > 0){
        this.authID = user[0].uuid_user;
      }
      console.log('testetst ' + this.authID);
      this.isWaited();
    });
  }
  isWaited(){
    this.saveChanges = this.formBuilder.group({
      name: new FormControl(),
      location: new FormControl(),
      birthdate: new FormControl(),
      email: new FormControl(),
      login: new FormControl(),
      description: new FormControl(),
    });

    this.userService.getUserById(this.authID).subscribe((data) => {
      this.profile = data[0];
      this.saveChanges.get('login').setValue(this.profile.user_login);
      this.saveChanges.get('name').setValue(this.profile.user_name);
      this.saveChanges.get('location').setValue(this.profile.user_location);
      this.saveChanges.get('email').setValue(this.profile.user_email);
      this.saveChanges.get('description').setValue(this.profile.user_description);

      return data;
    });
  }
  saveChange(){
    const login = this.saveChanges.get('login').value;
    const name = this.saveChanges.get('name').value;
    const location = this.saveChanges.get('location').value;
    const birthDate = this.saveChanges.get('birthdate').value;
    const email = this.saveChanges.get('email').value;
    const description = this.saveChanges.get('description').value;

    this.userService.editLogin(this.authID, login);
    this.new_profile = this.profile;
    this.new_profile.user_name = name;
    this.new_profile.user_location = location;
    this.new_profile.user_birthdate = birthDate;
    this.new_profile.user_email = email; // email unique ?
    this.userService.editInfos(this.authID, this.new_profile);
    this.new_profile = this.profile;
    this.new_profile.user_description = description;
    this.userService.editInfos(this.authID, this.new_profile);
    this.ngOnInit();
  }
  backToProfile(){
    this.router.navigate(['profile/profile/user']);
  }
}
