import { Component, OnInit } from '@angular/core';
import {User} from '../../models/Users.models';
import {Follow} from '../../models/Follow.models';
import {UsersService} from '../../services/users.service';
import {FollowService} from '../../services/follow.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profile-follows',
  templateUrl: './profile-follows.component.html',
  styleUrls: ['./profile-follows.component.css']
})
export class ProfileFollowsComponent implements OnInit {

  users: User[];
  follows: Follow[];
  
  paramLogin: string;

  constructor(private followService: FollowService,
              private userService: UsersService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.paramLogin = this.route.snapshot.params['id']; //even if I actually ask the login
    this.userService.getUserByLogin(this.paramLogin).subscribe(user => {
      this.followService.getFollows(user[0].uuid_user).subscribe((data) => {
        this.follows = data;
        this.users = [];
  
        data.forEach(follow => {
          this.userService.getUserById(follow.uuid_user_followed).subscribe((user) => {
            this.users.push(user[0]);
            this.users.sort((a,b) => a.user_login > b.user_login ? 1 : -1);
          });
        });
  
        return data;
      });
    });
  }
}
