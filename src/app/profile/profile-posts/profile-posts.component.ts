import {Component, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Post, PostStats, PostAndShares} from '../../models/Posts.models';
import {Partage} from '../../models/Partages.models';
import {Commentaire} from '../../models/Commentaires.model';
import {Observable, Subscription} from 'rxjs';
import {PostsService} from '../../services/posts.service';
import {PartageService} from '../../services/partage.service';
import {CommentService} from '../../services/comment.service';
import {LikeService} from '../../services/like.service';
import {Router} from '@angular/router';
import {UsersService} from '../../services/users.service';
import {User} from '../../models/Users.models';
import { faHeart } from '@fortawesome/free-solid-svg-icons/faHeart';
import { faComment } from '@fortawesome/free-solid-svg-icons/faComment';
import { faShare } from '@fortawesome/free-solid-svg-icons/faShare';
import { DomSanitizer } from '@angular/platform-browser';
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-profile-posts',
  templateUrl: './profile-posts.component.html',
  styleUrls: ['./profile-posts.component.css']
})
export class ProfilePostsComponent implements OnInit {
  authId = "";
  
  @Input() profileId: string;
  profile: User;

  posts: PostAndShares[];
  completePost: boolean[];
  nbPosts: number;
  nbShares: number;
  iPost = 0;
  iShare = 0;

  faHeart = faHeart;
  faComment = faComment;
  faShare = faShare;

  btnshareStatement: boolean;
  buttonLove = 'neutre';
  lovedPost: boolean;
  userSession: boolean; //ne sert pas encore mais
                    // devrait permettre de liker si session visiteur

  constructor(private postService: PostsService,
              private commentService: CommentService,
              private partageService: PartageService,
              private likeService: LikeService,
              private router: Router,
              private userService: UsersService,
              private sanitizer: DomSanitizer,
              private keycloak: KeycloakService) {
  }

  ngOnInit(): void {
    const userId = this.keycloak.getKeycloakInstance().subject.toString();
    this.userService.getUserByKeycloak(userId).subscribe(user => {
      console.log(user);
      if (user.length > 0){
        this.authId = user[0].uuid_user;
      }
      console.log('testetst ' + this.authId);
      this.isWaited();
    });
  }
  isWaited(){

    this.userSession = this.authId == this.profileId;
    this.posts = [];

    this.userService.getUserById(this.profileId).subscribe((user) => {
      this.profile = user[0];
      return user;
    });
    this.postService.getPostsByUser(this.profileId).subscribe((posts) => {
      this.nbPosts = posts.length;
      posts.forEach(post => {
        this.commentService.getNbCommentsFromPost(post.uuid_post).subscribe((countComment) => {
          this.partageService.getNbSharesOfPost(post.uuid_post).subscribe((countShare) => {
            this.likeService.getNbLikeForPost(post.uuid_post).subscribe(countLike => {
              this.posts.push(new PostAndShares(post, this.profile, undefined, undefined, post.post_date,
                  countComment[0].count,countShare[0].count, countLike[0].count));
              this.iPost += 1;
            });
          });
        });
        if(post.post_imagename != undefined){ //shall disappear
          this.postService.downloadFile(post.post_imagename).subscribe((data : Blob) => {
            const unsafeUrl = URL.createObjectURL(data)
            post.img = this.sanitizer.bypassSecurityTrustUrl(unsafeUrl);
          });
        }
      });

      return posts;
    });
    this.partageService.getSharesFromUser(this.profileId).subscribe((shares) => {
      this.nbShares = shares.length;
      shares.forEach(share => {
        if(share.uuid_post != undefined){
          this.postService.getPostById(share.uuid_post).subscribe((post) => {
            this.userService.getUserById(share.uuid_author_user).subscribe((user) => {
              this.commentService.getNbCommentsFromPost(share.uuid_post).subscribe(countComment => {
                this.partageService.getNbSharesOfPost(share.uuid_post).subscribe(countShare => {
                  this.likeService.getNbLikeForPost(share.uuid_post).subscribe(countLike => {
                    this.posts.push(new PostAndShares(post[0], user[0], share, this.profile, share.share_date,
                        countComment[0].count,countShare[0].count, countLike[0].count));
                    this.iShare += 1;
                  });

                });
              });
            });
            if(post[0].post_imagename != undefined){ //shall disappear
              this.postService.downloadFile(post[0].post_imagename).subscribe((data : Blob) => {
                const unsafeUrl = URL.createObjectURL(data)
                post[0].img = this.sanitizer.bypassSecurityTrustUrl(unsafeUrl);
              });
            }
          });
        }
      });
    });
    this.postService.emitPosts();
  }
  checkIfComplete(i : number){
    if(this.posts[i].share == undefined){ //post : need counts, share, share_user, author
      const haveAuthor = this.posts[i].author != undefined;
      const haveNbLike = this.posts[i].nbLikes != undefined;
      const haveNbComment = this.posts[i].nbComments != undefined;
      const haveNbShare = this.posts[i].nbShares != undefined;
      if(haveAuthor && haveNbComment && haveNbLike && haveNbShare){
        this.completePost[i] = true;
      }
    }else if(this.posts[i].share.uuid_post != undefined){ //share: need everythings
      const havePost = this.posts[i].post != undefined;
      const haveAuthor = this.posts[i].author != undefined;
      const haveShareUser = this.posts[i].share_user != undefined;
      const haveNbLike = this.posts[i].nbLikes != undefined;
      const haveNbComment = this.posts[i].nbComments != undefined;
      const haveNbShare = this.posts[i].nbShares != undefined;
      if(haveAuthor && haveNbComment && haveNbLike && haveNbShare && havePost && haveShareUser){
        this.completePost[i] = true;
      }
    }else{ //empty share : need share_user
      if(this.posts[i].share_user != undefined){
        this.completePost[i] = true;
      }
    }
  }

  getSortedPosts() : PostAndShares[]{
    return this.posts.sort((a,b) => new Date(b.date).getTime() - new Date(a.date).getTime());
  }

  // tslint:disable-next-line:typedef
  onNewPost(){
    this.router.navigate(['/posts', 'new']);
  }
  // tslint:disable-next-line:typedef
  onDeletePost(post: Post) {
    this.postService.removePost(post);
  }

  onDeleteShare(share: Partage){
    this.partageService.deleteFollow(share.uuid_share);
  }
  // tslint:disable-next-line:typedef
  onShare(post: PostAndShares){
    this.partageService.addPartage(post.post.uuid_post, this.authId, post.post.uuid_user);
    this.btnshareStatement = false;
  }
  // tslint:disable-next-line:typedef
  onLove(post: PostAndShares){
    if(this.lovedPost){
      this.lovedPost = false;
      post.nbLikes -= 1;
      post.post.post_nb_like -= 1;
      this.likeService.deleteLike(this.authId, post.post.uuid_post);
      this.ngOnInit();
    }else{
      this.lovedPost = true;
      post.nbLikes += 1;
      post.post.post_nb_like += 1;
      this.likeService.addLike(this.authId, post.post.uuid_post);
      this.ngOnInit();
    }
  }
  // tslint:disable-next-line:typedef
  onDontlove(post: Post){
    this.postService.dontLove(post);
    this.buttonLove = 'dislike';
  }
  onProfile(login: string){
    this.router.navigate(['profile/profile/' + login]);
  }

}
