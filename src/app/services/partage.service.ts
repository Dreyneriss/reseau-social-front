import { Injectable } from '@angular/core';
import {urls} from '../../environments/urls';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Partage} from '../models/Partages.models';
import {Count} from '../models/Count.model';

@Injectable({
  providedIn: 'root'
})
export class PartageService {
  
  private baseUrl = urls.partage;

  constructor(private http: HttpClient) { }

  getShares(): Observable<Partage[]> {
    return this.http.get<Partage[]>(this.baseUrl + '/getPartages');
  }
  getShareFromId(id: string): Observable<Partage[]> {
    return this.http.get<Partage[]>(this.baseUrl + '/getPartages/partage/'+id);
  }
  getSharesFromUser(user: string): Observable<Partage[]> {
    return this.http.get<Partage[]>(this.baseUrl + '/getPartages/shareUser/' + user);
  }
  getSharesOfUser(user: string) : Observable<Partage[]>{
    return this.http.get<Partage[]>(this.baseUrl + '/getPartages/authorUser/' + user);
  }
  getSharesOfPost(post: string) : Observable<Partage[]>{
    return this.http.get<Partage[]>(this.baseUrl + '/getPartages/post/' + post);
  }

  getNbSharesOfPost(post: string) : Observable<Count[]>{
    return this.http.get<Count[]>(this.baseUrl + '/getPartageCount/post/' + post);
  }
  
  addPartage(post : string, user : string, author : string){
    let body = {
      "idPost" : post,
      "idShareUser" : user,
      "idAuthorUser": author
    };
    return this.http.post(this.baseUrl+'/addPartage', body).subscribe(
      () => {
        console.log('Follow added !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  emptyPartageFromPost(idPost : string){
    let body = {
      "idPost" : idPost
    };
    return this.http.put(this.baseUrl+'/emptyPartage/post', body).subscribe(
      () => {
        console.log('Share emptied !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteFollow(id : string){
    return this.http.delete(this.baseUrl+'/deletePartage/partage/'+id).subscribe(
      () => {
        console.log('Share deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteFollowFromUser(id : string){
    return this.http.delete(this.baseUrl+'/deletePartage/user/'+id).subscribe(
      () => {
        console.log('Share deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

}
