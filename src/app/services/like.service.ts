import { Injectable } from '@angular/core';
import {Like} from '../models/Likes.models';
import {Count} from '../models/Count.model';
import {Observable, Subject} from 'rxjs';
import {urls} from '../../environments/urls';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  private likeUrl = urls.like;
  likeSubject = new Subject<Like[]>();

  constructor(private http: HttpClient) { }

  getLikes(): Observable<Like[]>{
    return this.http.get<Like[]>(this.likeUrl+'/getLikes/');
  }

  getLikesFromUser(userId): Observable<Like[]>{
    return this.http.get<Like[]>(this.likeUrl+'/getLikes/user/'+userId);
  }

  getLikesFromPost(postId): Observable<Like[]>{
    return this.http.get<Like[]>(this.likeUrl+'/getLikes/post/'+postId);
  }

  getLikesFromPostAndUser(postId, userId): Observable<Like[]>{
    return this.http.get<Like[]>(this.likeUrl+'/getLikes/post/'+postId+'/user/'+userId);
  }

  getNbLikeForPost(postId): Observable<Count[]>{
    return this.http.get<Count[]>(this.likeUrl + '/getLikeCount/post/' + postId);
  }

  addLike(userId, postId){
    let body = {
      "idUser" : userId,
      "idPost" : postId
    };
    console.log("Adding a like.");
    console.log("user :", body.idUser);
    console.log("post :", body.idPost);
    return this.http.post(this.likeUrl+'/addLike', body).subscribe(
      () => {
        console.log('Like added !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteLike(userId, postId){
    return this.http.delete(this.likeUrl+'/deleteLike/user/'+userId+'/post/'+postId).subscribe(
      () => {
        console.log('like deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteLikesFromPost(postId){
    return this.http.delete(this.likeUrl+'/deleteLike/post/'+postId).subscribe(
      () => {
        console.log('likes deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteLikesFromUser(userId){
    return this.http.delete(this.likeUrl+'/deleteLike/user/'+userId).subscribe(
      () => {
        console.log('likes deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }
}
