import { Injectable } from '@angular/core';
import {urls} from '../../environments/urls';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/Users.models';
import {Post} from '../models/Posts.models';
import {KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private baseUrl = urls.users;
  users: User[] = [];
  userSubject = new Subject<User[]>();
  userUuid = '';

  constructor(private http: HttpClient, private keycloak: KeycloakService) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + '/getUsers');
  }
  getUserById(id: string): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + '/getUsers/user/' + id);
  }
  getUserByKeycloak(id: string): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + '/getUsers/keycloak/' + id);
  }
  getUserByLogin(login: string): Observable<User[]>{
    return this.http.get<User[]>(this.baseUrl + '/getUsers/login/' + login);
  }

  addUser(first_name: string, last_name: string, login: string, email: string, keycloak: string) {
    let body = {
      "name": first_name + " " + last_name,
      "login": login,
      "email": email,
      "password": '',
      "imageURL": '',
      "location": '',
      "birthDate": '',
      "id_keycloak": keycloak
    };
    return this.http.post(this.baseUrl + '/addUser', body).subscribe(
        () => {
          console.log('User added !');
        },
        (error) => {
          console.log('Error ! : ' + error);
        }
    );
  }
  editLogin(userId, login){
    this.http.get<User[]>(this.baseUrl+'/getUsers/login/'+login).subscribe((user) => {
      if(user.length > 0){
        console.log("Login ", login, " already exist. (how to warn the user ?)")
      }else{
        let body = {
            "id" : userId,
            "login" : login
        };
        console.log("Doing put request.")
        return this.http.put<User>(this.baseUrl+'/editUser/login', body).subscribe(
          () => {
            console.log('Login edited !');
          },
          (error) => {
            console.log('Error ! : ' + error);
          }
        );
      }
    });
  }

  editInfos(userId, profile : User){
    let body = {
      "id" : userId,
      "name" : profile.user_name,
      "password" : profile.user_password,
      "imageURL" : profile.user_imageurl,
      "location" : profile.user_location,
      "birthDate" : profile.user_birthdate,
      "description" : profile.user_description,
      "email" : profile.user_email
    };
    return this.http.put<User>(this.baseUrl+'/editUser', body).subscribe(
      () => {
        console.log('User edited !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }
}
