import { Injectable } from '@angular/core';
import {Follow, FollowCount} from '../models/Follow.models';
import {Observable, Subject} from 'rxjs';
import {urls} from '../../environments/urls';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  private followUrl = urls.follows;
  followSubject = new Subject<Follow[]>();

  constructor(private http: HttpClient) { }

  getFollowers(userId): Observable<Follow[]>{
    return this.http.get<Follow[]>(this.followUrl+'/listFollowers/'+userId);
  }

  getFollows(userId): Observable<Follow[]>{
    return this.http.get<Follow[]>(this.followUrl+'/listFollowed/'+userId);
  }

  getNbFollower(userId): Observable<FollowCount[]>{
    return this.http.get<FollowCount[]>(this.followUrl+'/followerCount/'+userId); 
  }

  getNbFollow(userId): Observable<FollowCount[]>{
    return this.http.get<FollowCount[]>(this.followUrl+'/followCount/'+userId); 
  }

  addFollow(follower, user_followed){
    let body = {
      "uuid_follower" : follower,
      "uuid_user_followed" : user_followed
    };
    return this.http.post<Follow>(this.followUrl+'/createFollow', body).subscribe(
      () => {
        console.log('Follow added !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteFollow(follower, user_followed){
    return this.http.delete(this.followUrl+'/removeFollow/'+follower+'/'+user_followed).subscribe(
      () => {
        console.log('Follow deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }


}
