import { Injectable } from '@angular/core';
import {Post, PictureId} from '../models/Posts.models';
import {Observable, Subject} from 'rxjs';
import {urls} from '../../environments/urls';
import {HttpClient} from '@angular/common/http';
import {stringify} from '@angular/compiler/src/util';
import {CommentService} from './comment.service'
import {LikeService} from './like.service'
import {PartageService} from './partage.service'
import {KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private baseUrl = urls.posts;
  private s3Url = urls.s3;
  posts: Post[] = [];
  postSubject = new Subject<Post[]>();


  constructor(private commentService: CommentService,
    private partageService: PartageService,
    private likeService: LikeService,
    private http: HttpClient,
    private keycloak: KeycloakService) {}
  // tslint:disable-next-line:typedef

  emitPosts(){
    this.postSubject.next(this.posts);
  }
  // tslint:disable-next-line:typedef
  savePost(){
    // firebase.database().ref('/posts').set(this.posts);
  }
  // tslint:disable-next-line:typedef
  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/getPosts');
  }
  // tslint:disable-next-line:typedef
  getPostsByUser(id): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/getPosts/user/' + id);
  }
  // tslint:disable-next-line:typedef
  getPostById(id): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/getPosts/post/' + id);
  }

  editPostContent(post : Post){
    let body = {
      "id" : post.uuid_post,
      "content" : post.post_content
    };
    return this.http.put<Post>(this.baseUrl + '/editPost', body).subscribe(
      () => {
        console.log('Post content edited !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  // tslint:disable-next-line:typedef
  createNewPost(user: string, content: string, imageId: string){
    let body = {
      "user" : user,
      "content" : content,
      "imageId": imageId
    };
    return this.http.post(this.baseUrl+'/addPost', body).subscribe(
      () => {
        console.log('Post added !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }
  // tslint:disable-next-line:typedef
  removePost(post: Post) {
    this.likeService.deleteLikesFromPost(post.uuid_post);
    this.commentService.deleteCommentsFromPost(post.uuid_post);
    this.partageService.emptyPartageFromPost(post.uuid_post);
    return this.http.delete(this.baseUrl+'/deletePost/post/'+post.uuid_post).subscribe(
      () => {
        console.log('Post deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  // tslint:disable-next-line:typedef
  uploadFile(file: File, authId: string, content: string) {
    return new Promise(
        (resolve, reject) => {
          const formData = new FormData()
          formData.append('file', file)
          this.http.post<PictureId>(this.s3Url+'/add', formData).subscribe(res => {
            console.log(res.name);
            this.createNewPost(authId, content, res.name);
          });
          // const almostUniqueFileName = Date.now().toString();
          // const upload = firebase.storage().ref()
          //     .child('images/' + almostUniqueFileName + file.name)
          //     .put(file);
          // upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          //     () => {
          //       console.log('chargement ...');
          //     },
          //     (error) => {
          //       console.log('Erreur de chargement : ' + error);
          //       reject();
          //     },
          //     () => {
          //       resolve(upload.snapshot.ref.getDownloadURL());
          //     });
            resolve(true);
        }
    );
  }

  downloadFile(fileName : string){
    return this.http.get(this.s3Url + '/get/' + fileName, { responseType: 'blob' as 'json' });
  }

  // tslint:disable-next-line:typedef
  lovePost(post: Post){
    // post.like += 1;
    // this.savePost();
    // this.emitPosts();
  }
  // tslint:disable-next-line:typedef
  dontLove(post: Post){
    // post.dislike += 1;
    // this.savePost();
    // this.emitPosts();
  }
}
