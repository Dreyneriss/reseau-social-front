import { Injectable } from '@angular/core';
import {urls} from '../../environments/urls';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Commentaire} from '../models/Commentaires.model';
import {Count} from '../models/Count.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private baseUrl = urls.comments;

  constructor(private http: HttpClient) { }

  getComments(): Observable<Commentaire[]> {
    return this.http.get<Commentaire[]>(this.baseUrl + '/getComments');
  }
  getCommentsFromPost(post: string): Observable<Commentaire[]> {
    return this.http.get<Commentaire[]>(this.baseUrl + '/getComments/post/' + post);
  }
  getCommentsFromUser(user: string) : Observable<Commentaire[]>{
    return this.http.get<Commentaire[]>(this.baseUrl + '/getComments/user/' + user);
  }

  getNbCommentsFromPost(post: string): Observable<Count[]> {
    return this.http.get<Count[]>(this.baseUrl + '/getCommentCount/post/' + post);
  }
  getNbCommentsFromUser(user: string): Observable<Count[]> {
    return this.http.get<Count[]>(this.baseUrl + '/getCommentCount/user/' + user);
  }

  editComment(comment : Commentaire){
    let body = {
      "id" : comment.uuid_comment,
      "comment" : comment.comment
    };
    return this.http.put<Commentaire>(this.baseUrl+'/editComment', body).subscribe(
      () => {
        console.log('Comment edited !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  postComment(idUser:string, idPost:string, comment:string){
    let body = {
      "user" : idUser,
      "post" : idPost,
      "comment" : comment
    };
    return this.http.post<Commentaire>(this.baseUrl+'/addComment', body).subscribe(
      () => {
        console.log('Comment added !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteComment(id:string){
    return this.http.delete(this.baseUrl+'/deleteComment/comment/'+id).subscribe(
      () => {
        console.log('Comment deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteCommentsFromPost(id:string){
    return this.http.delete(this.baseUrl+'/deleteComment/post/'+id).subscribe(
      () => {
        console.log('Comments deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }

  deleteCommentsFromUser(id:string){
    return this.http.delete(this.baseUrl+'/deleteComment/user/'+id).subscribe(
      () => {
        console.log('Comments deleted !');
      },
      (error) => {
        console.log('Error ! : ' + error);
      }
    );
  }
}
