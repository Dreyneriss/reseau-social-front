import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAuth: boolean;
  mobile = false;
  userName = '';

  constructor( private router: Router, private keycloakService: KeycloakService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    if (window.screen.width <= 1000) { // 768px portrait
      this.mobile = true;
    }
    this.userName = this.keycloakService.getUsername();
  }
  onNewPost(): void{
    this.router.navigate(['/posts', 'new']);
  }

}
