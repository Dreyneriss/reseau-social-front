//Ce fichier va contenir les urls des api

import { PostsService } from 'src/app/services/posts.service';
import {environment, ports} from './environment';

export const urls = {
    users: 'http://api.user.picarrot.live/user',
    posts: 'http://api.post.picarrot.live/post',
    notification: environment.sourceUrl + ports.apiNotification + '/notification',
    like: 'http://api.like.picarrot.live/like',
    partage: 'http://api.partage.picarrot.live/partage',
    comments: 'http://api.comment.picarrot.live/comment',
    follows: 'http://api.follow.picarrot.live/follow',
    s3: 'http://api.s3.picarrot.live/s3'
};


/*export const urls = {
    users: environment.sourceUrl + ports.apiUser + '/user',
    posts: environment.sourceUrl + ports.apiPost + '/post'
    notification: environment.sourceUrl + ports.apiNotification + '/notification',
    like: environment.sourceUrl + ports.apiLike + '/like',
    partage: environment.sourceUrl + ports.apiPartage + '/partage',
    comments: environment.sourceUrl + ports.apiComment + '/comment',
    follows: environment.sourceUrl + ports.apiFollow + '/follow',
    s3: environment.sourceUrl + ports.apiS3 + '/s3'
};*/
