// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {HttpHeaders} from '@angular/common/http';

export const optionRequete = {
  header: new HttpHeaders({
    'Access-Control-Allow-Origin' : '*',
    'Content-Security-Policy' : 'frame-ancestors "self" http://app3.picarrot.live/'
  })
};
export const environment = {
  production: false,
  sourceUrl: 'http://app3.picarrot.live',
  version: '1.0.1 dev',
};

export const ports = {
  apiUser: 8081,
  apiPost: 8082,
  apiNotification: 8083,
  apiLike: 8084,
  apiPartage: 8085,
  apiComment: 8086,
  apiFollow: 8087,
  apiS3: 8088
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
